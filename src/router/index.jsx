import { createBrowserRouter } from "react-router-dom";
import { App } from "../App";
import { Cart } from "../pages/cart";
import { Favorites } from "../pages/favorites";
import { Shop } from "../pages/shop";

export const router = createBrowserRouter([
  {
    element: <App />,
    path: "/",
    children: [
      {
        element: <Shop />,
        index: true,
      },
      {
        element: <Cart />,
        path: "Cart",
      },
      {
        element: <Favorites />,
        path: "favorites",
      },
    ],
  },
]);
