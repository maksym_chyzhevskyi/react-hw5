import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Layout } from "./layout";
import { getData } from "./redux/extraReducers/getData";
import { setFavorites, setProductsInCart } from "./redux/reducers/products";

const favoriteIdsFromStorage = localStorage.getItem("favorites");

const cartIdsFromStorage = localStorage.getItem("Cart");

export const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getData());
  }, [dispatch]);

  useEffect(() => {
    if (favoriteIdsFromStorage) {
      dispatch(setFavorites(JSON.parse(favoriteIdsFromStorage)));
    }

    if (cartIdsFromStorage) {
      dispatch(setProductsInCart(JSON.parse(cartIdsFromStorage)));
    }
  }, [dispatch]);

  return <Layout />;
};
