import React from "react";
import { GoodsSection } from "../goodsSection";
import styles from "../../styles/main.module.scss";

export const Main = () => {
  return (
    <div className={styles.Main__row}>
      <GoodsSection />
    </div>
  );
};
