import React from "react";
import { useSelector } from "react-redux";
import { CartIco } from "../icons/cartIco";
import { FavIco } from "../icons/favIco";
import { HeaderMenu } from "../headerMenu";
import styles from "../../styles/header.module.scss";
import { NavLink } from "react-router-dom";

const getProducts = (state) => state.products;
export const Header = () => {
  const { favorites, productsInCart } = useSelector(getProducts);

  const countFavorites = favorites.length;
  const countProductsInCart = productsInCart.reduce(
    (value, item) => value + item.count,
    0
  );

  return (
    <div className={styles.Header__row}>
      <HeaderMenu />

      <div className={styles.HeaderActions}>
        <div className={styles.HeaderCart}>
          <NavLink to="/cart">
            <CartIco width={26} fill={"#1c8646"} />
          </NavLink>
          <span className={styles.cartValue}>{countProductsInCart}</span>
        </div>
        <div className={styles.HeaderFavorites}>
          <NavLink to="/favorites">
            <FavIco width={26} fill={"#ffda12"} />
          </NavLink>
          <span className={styles.favoritesValue}>{countFavorites}</span>
        </div>
      </div>
    </div>
  );
};
