import React from "react";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import {
  handleAddCartClick,
  handleDeleteCartClick,
  handleFavoritesClick,
} from "../../redux/reducers/products";
import { openModal } from "../../redux/reducers/modal";
import { FavIco } from "../icons/favIco";
import { FavEmptyIco } from "../icons/favEmptyIco";
import { MinusIco } from "../icons/minusIco";
import { PlusIco } from "../icons/plusIco";
import styles from "../../styles/product.module.scss";

const selectorGetProducts = (state) => state.products;

export const Product = ({ product }) => {
  const dispatch = useDispatch();

  const { favorites, productsInCart } = useSelector(selectorGetProducts);
  const { id, name, price, imgUrl, article, color } = product;

  const isFavorites = favorites.includes(id);

  const searchItem = productsInCart.find((item) => item.id === id);
  const isCart = searchItem?.count;

  const addToCart = dispatch(() => handleAddCartClick(id));
  const deleteToCart = dispatch(() => handleDeleteCartClick(id));

  const handleAddProduct = () => {
    dispatch(
      openModal({
        header: "Do you want to add the item to the cart?",
        closeButton: true,
        name,
        price,
        action: [
          {
            text: "OK",
            backgroundColor: "#1c8646",
            actionCart: addToCart,
          },
          {
            text: "CANCELL",
            backgroundColor: "#1c8646",
            actionCart: null,
          },
        ],
      })
    );
  };

  const handleDeleteProduct = () => {
    dispatch(
      openModal({
        header: "Do you want to remove the product from the cart?",
        closeButton: true,
        name,
        price,
        action: [
          {
            text: "OK",
            backgroundColor: "#cc1934",
            actionCart: deleteToCart,
          },
          {
            text: "CANCELL",
            backgroundColor: "#cc1934",
            actionCart: null,
          },
        ],
      })
    );
  };

  return (
    <>
      <div className={styles.ProductContainer}>
        <div className={styles.Product}>
          <span className={styles.favorites}>
            {isFavorites && (
              <span
                onClick={() => {
                  dispatch(handleFavoritesClick(id));
                }}
              >
                <FavIco width={34} fill={"#ffda12"} />
              </span>
            )}

            {!isFavorites && (
              <span
                onClick={() => {
                  dispatch(handleFavoritesClick(id));
                }}
              >
                <FavEmptyIco width={34} fill={"#ffda12"} />
              </span>
            )}
          </span>
          <div className={styles.body}>
            <div className={styles.img}>
              <img src={`./img/${imgUrl}`} alt="" />
            </div>
            <div className={styles.title}>{name}</div>
            <div className={styles.article}>
              <span className={styles.articleText}>Article</span>:&nbsp;
              <span className={styles.articleValue}>{article}</span>
            </div>
            <div className={styles.color}>
              <span className={styles.colorText}>Color</span>:&nbsp;
              <span className={styles.colorValue}>{color}</span>
            </div>
          </div>
          <div className={styles.price}>
            <span className={styles.sum}>{price}</span>
            <span className={styles.currency}> usd</span>
          </div>
          <div className={styles.bottom}>
            <div className={styles.actions}>
              <span className={styles.inCartTxt}>cart:</span>
              <span className={styles.inCartCount}>{isCart || 0}</span>
              <span className={styles.cart}>
                {isCart && (
                  <span onClick={handleDeleteProduct}>
                    <MinusIco width={20} fill={"#cc1934"} />
                  </span>
                )}
                <span onClick={handleAddProduct}>
                  <PlusIco width={20} fill={"#1c8646"} />
                </span>
              </span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

Product.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string,
    price: PropTypes.number,
    imgUrl: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
  }).isRequired,
};

Product.defaultProps = {
  product: {
    name: "",
    price: 0,
    imgUrl: "",
    article: "",
    color: "",
  },
};
